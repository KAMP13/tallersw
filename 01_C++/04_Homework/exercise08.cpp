#include <iostream>
#include <math.h>
/*8.- Calcular el monto final, dados como datos el Capital 
Inicial, el  tipo de Inter�s, el numero de periodos por a�o, 
y el numero de a�os de  la inversi�n. El c�lculo del capital 
final se basa en la formula del inter�s compuesto.
				M = C(1+i/N)
	Donde:
	M = Capital final o Monto,	C = Capital Inicial,	
	i = Tipo de inter�s nominal
	N = Numero de periodos por a�o,		T = Numero de a�os
- Si un cliente deposita al Banco la cantidad de $10,000 a 
inter�s compuesto con una tasa del 8% anual.  �Cual ser� el 
monto que recaude despu�s de 9 a�os? 
 - �Cuanto debe cobrar el cliente dentro de 3 a�os si 
 deposita  $ 100,000. al 9% anual  y capitaliz�ndose los 
 intereses bimestralmente?   
*/
using namespace std;
int main(){
	
	//Declaration
	double monto, capital, tipoDeInteres, numeroDePeriodos, 
	numeroDeAnios, porAnios, interes;
	
	//initialize
	monto = 0;
	numeroDeAnios = 0;
	
	//Display phrase 
	cout<<"Digite tipo de interes en porcentaje: ";
	cin>>tipoDeInteres;
	cout<<"Digite capital: ";
	cin>>capital;
	cout<<"Digite numero de periodos: ";
	cin>>numeroDePeriodos;
	cout<<"Digite numero de a�os: ";
	cin>>numeroDeAnios;
	
	//operador
	tipoDeInteres = interes/100;
	monto = capital*(1+(tipoDeInteres/numeroDePeriodos));
	numeroDeAnios = monto * numeroDeAnios;
	
	//Display phrase
	cout<<"\n\rmonto: "<<monto<<endl;
	cout<<"\n\rmonto por A�os: "<<numeroDeAnios<<endl;
	
	
	return 0;
	
}
