#include <iostream>
#include <math.h>
/* 10.-  Un millonario exc�ntrico ten�a tres hijos: Carlos, Jos� y Marta. 
Al morir dej� el siguiente legado: A Jos� le dej� 4/3 de lo que le dej� a Carlos. 
A Carlos le dej� 1/3 de su fortuna. A Marta le dejo la mitad de lo que le dej� a Jos�. 
Preparar un algoritmo para darle la suma a repartir e imprima cuanto le toc� a cada uno.  
*/
using namespace std;
int main(){
	
	//Declaration
	double carlos, jose, marta, total;
	
	//initialize
	carlos = 0;
	jose = 0;
	marta = 0;
	
	//Display phrase
	cout<<"Digite total de la herencia: ";
	cin>>total;
	
	//operador
	carlos = total/3;
	jose = (4*carlos)/3 ;
	marta = (jose/2);
	
	
	//Display phrase
	cout<<"\n\rCarlos: "<<carlos<<endl;
	cout<<"\n\rJose: "<<jose<<endl;
	cout<<"\n\rMarta: "<<marta<<endl;
	
	
	return 0;
	
}
