#include <iostream>
#include <math.h>
/* 19.- Dados como datos las coordenadas de los tres puntos P1, P2, P3 que corresponden 
a los v�rtices de un triangulo, calcule su per�metro y �rea.
*/
using namespace std;
int main(){
	
	//Declaration
	double a1,a2,b1,b2,c1,c2;//A(a1,a2) B(b1,b2) C(c1,c2)
	double perimetro;
	double d1,d2,d3;
	double area;
	
	
	//initialize
	
	//Display phrase
	cout<<"Digite el primer punto: ";
	cin>>a1>>a2;
	cout<<"Digite el segundo punto: ";
	cin>>b1>>b2;
	cout<<"Digite el tercer punto: ";
	cin>>c1>>c2;
	
	//operador
	d1=sqrt(pow(b1-a1,2)+pow(b2-a2,2));//a b
	d2=sqrt(pow(c1-a1,2)+pow(c2-a2,2));//a c
	d3=sqrt(pow(c1-b1,2)+pow(c2-b2,2));//b c
	perimetro = d1 + d2 + d3;
	
	area = (((a1*b2)+(b1*c2)+(c1*a2))-((a1*c2)+(c1*b2)+(b1*a2)))/2;
	
	if(area<0){
		area=area*(-1);
	}
	else{
		area;
	}
    	
	
	//Display phrase
	cout<<"\n\rPerimetro: "<<perimetro<<endl;
	cout<<"\n\rArea: "<<area<<endl;
	
	
	return 0;
	
}
