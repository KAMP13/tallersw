#include <iostream>
/*6.- Desglosar cierta cantidad de segundos a su equivalente 
en d�as, horas, minutos y segundos.
*/
using namespace std;
int main(){
	
	//Declaration
	int segundos, dias, horas, minutos;
	
	//initialize
	dias = 0;
	horas = 0;
	minutos = 0;
	segundos = 0;
	
	//Display phrase 
	cout<<"Digite numero de segundos: ";
	cin>>segundos;
	
	//operador
	dias = segundos/86400;
	horas = segundos/3600;
	minutos = segundos/600;
	segundos = segundos;
	
	//Display phrase
	cout<<"\nEquivalente en Dias: "<<dias<<endl;
	cout<<"\n\rEquivalente en Horas: "<<horas<<endl;
	cout<<"\n\rEquivalente en Minutos: "<<minutos<<endl;
	cout<<"\n\rEquivalente en Segundos: "<<segundos<<endl;
	
	return 0;
	
}
