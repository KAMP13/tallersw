/**
* @author Kevin Martinez (kampzen13@gmail.com)
		*2.- Un maestro desea saber que porcentaje de hombres 
		y que porcentaje de mujeres hay en un grupo de 
		estudiantes.
 * @version 1.0
 * @date 05.12.2021
 * 
*/

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	// Declaration and Initialization of variables
	float 	terminalInputNumberOfMen = 0;			//Entrada de hombres a travez del terminal
	int		terminalInputNumberOfWomen = 0;			//Entrada de mujeres a travez del terminal
	float	porcentageOfMen = 0;					//Pocentaje de hombres
	float	porcentageOfWomen = 0;					//Porcentaje de Mujeres
	int		total = 0;								//Cantidad total de mujeres y hombres
	
	
	// Ask data throught the terminal
	cout<<"============= Insert Data ===============\r\n";
	cout<<"\tWrite the number of men: ";
	cin>>terminalInputNumberOfMen;
	cout<<"\tWrite the number of women: ";
	cin>>terminalInputNumberOfWomen;
	
	// Calculation
	total = terminalInputNumberOfMen+terminalInputNumberOfWomen;		//total = cantidad_hombres + cantidad_mujeres
	porcentageOfMen = (terminalInputNumberOfMen*100)/total;				//porcentage_hombres = (cantidad_hombres * 100) / total
	porcentageOfWomen = (terminalInputNumberOfWomen*100)/total;			//porcentage_mujeres = (cantidad_mujeres * 100) / total
	
	// Show results in terminal
	cout<<"\r\n============= Show Results ===============\r\n";
	cout<<"\nPercentage of men: "<<porcentageOfMen<<endl;
	cout<<"\n\rPercentage of women: "<<porcentageOfWomen<<endl;
	
	return 0;
	
}
