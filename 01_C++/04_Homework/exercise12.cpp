#include <iostream>
#include <math.h>
/* 12.- Escriba un programa para calcular el tiempo transcurrido, 
en minutos, necesario para hacer un viaje. 
La ecuaci�n es tiempo transcurrido = distancia total/velocidad promedio. 
Suponga que la distancia est� en kil�metros y la velocidad en kil�metros/hora. 
*/
using namespace std;
int main(){
	
	//Declaration
	double tiempo;
	double distTotal;
	double veloPromedio;
	
	
	//initialize
	tiempo = 0;
	
	//Display phrase
	cout<<"Digite distancia total: ";
	cin>>distTotal;
	cout<<"Digite velocidad promedio: ";
	cin>>veloPromedio;
	
	//operador
	tiempo = distTotal/veloPromedio;
	tiempo = tiempo*60;
	
	
	//Display phrase
	cout<<"\n\rTiempo en minutos: "<<tiempo<<endl;
	
	return 0;
	
}
