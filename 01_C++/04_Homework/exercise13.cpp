#include <iostream>
#include <math.h>
/* 13.- Un tonel es un recipiente, generalmente de madera, 
muy utilizado para almacenar y mejorar un vino. La forma de 
un tonel es muy caracter�stica y es un cilindro en el que 
la parte central es m�s gruesa, es decir, tiene un di�metro 
mayor que los extremos. Escriba un programa que lea las medidas 
de un tonel y nos devuelva su
capacidad, teniendo en cuenta que el volumen (V) de un tonel 
viene dado por la siguiente f�rmula: V = ? l a2 donde:
 l    es la longitud del tonel, su altura. a = d/2 + 2/3(D/2 - d/2)
d   es el di�metro del tonel en sus extremos.
D  es el di�metro del tonel en el centro: D>d
Nota: Observe que si las medidas se dan en cent�metros 
el resultado lo obtenemos en cent�metros c�bicos.

*/
using namespace std;
int main(){
	
	//Declaration
	double volumen;
	double i;//longitud      
	double a;
	double diamExtremos;
	double diamCentro;
	
	
	//initialize
	a = 0;
	volumen = 0;
	
	//Display phrase
	cout<<"Digite longituf del tonel: ";
	cin>>i;
	cout<<"Digite diametro en los extremos: ";
	cin>>diamExtremos;
	cout<<"Digite diametro en el centro: ";
	cin>>diamCentro;
	
	//operador

	a = (diamExtremos/2)+(2/3)*(diamExtremos/2-diamCentro/2);
	volumen = M_PI*i*pow(a,2);
	
	//Display phrase
	cout<<"\n\rVolumen: "<<volumen<<endl;
	
	return 0;
	
}
