/**
* @author Kevin Martinez (kampzen13@gmail.com)
	*3.- Queremos conocer los datos estad�sticos de una 
		asignatura, por lo tanto, necesitamos un algoritmo 
		que lea el n�mero de desaprobados, aprobados, notables 
		y sobresalientes de una asignatura, y nos devuelva:
		a. El tanto por ciento de alumnos que han superado 
		la asignatura.
		b. El tanto por ciento de desaprobados, aprobados, 
		notables y sobresalientes de la asignatura.
 * @version 1.0
 * @date 05.12.2021
 * 
*/

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	// Declaration and Initialization of variables
	int 	desaprobados = 0;			//cantidad de desaprobados
	int 	aprobados = 0;				//cantidad de aprobados
	int		notables = 0;				//cantidad de notables
	int		sobresalientes = 0;			//cantidad de sobresalientes
	int		total = 0;					//total
	int		superado = 0;				//cantidad de superados
	float	porcSuperado = 0;			//porcentaje de superados
	float	porcDesaprobados = 0;		//porcentaje de desaprobados
	float	porcAprobados = 0;			//porcentaje de aprobados
	float	porcNotables = 0;			//porcentaje de notables
	float	porcSobresalientes = 0;		//porcentaje de sobresalientes
	
	
	// Ask data throught the terminal
	cout<<"=================== Insert Data =====================\r\n";
	cout<<"\tWrite the number of desaprobed: ";
	cin>>desaprobados;
	cout<<"\tWrite the number of aprobed: ";
	cin>>aprobados;
	cout<<"\tDigite number of notables: ";
	cin>>notables;
	cout<<"\tDigite number of outstanding: ";
	cin>>sobresalientes;
	
	
	// Calculation
	total = desaprobados+aprobados+notables+sobresalientes;
	superado = aprobados+notables+sobresalientes;
	porcSuperado = (superado*100)/total;
	porcDesaprobados = (desaprobados*100)/total;
	porcAprobados = (aprobados*100)/total;
	porcNotables = (notables*100)/total;
	porcSobresalientes = (sobresalientes*100)/total;
	
	
	// Show results in terminal
	cout<<"\r\n============= Show Results ===============\r\n";
	cout<<"\nPorcentaje que han superado la asignatura: "<<porcSuperado<<endl;
	cout<<"\n\rPorcentaje de desaprobados: "<<porcDesaprobados<<endl;
	cout<<"\n\rPorcentaje de aprobados: "<<porcAprobados<<endl;
	cout<<"\n\rPorcentaje de notables: "<<porcNotables<<endl;
	cout<<"\n\rPorcentaje de sobresalientes: "<<porcSobresalientes<<endl;
	
	return 0;
	
}
