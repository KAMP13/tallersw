#include <iostream>
#include <math.h>
/* 14. Modifique el programa anterior para que, suponiendo que 
las medidas de entrada son dadas en cent�metros, el resultado lo 
muestre en: litros, cent�metros c�bicos y metros c�bicos. 
Recuerde que 1 litro es equivalente a un dec�metro c�bico. 
Indique siempre la unidad de medida empleada.
*/
using namespace std;
int main(){
	
	//Declaration
	double volumen;
	double i;//longitud      
	double a;//contanste
	double diamExtremos;//diametro de los extremos
	double diamCentro;//diametro del centro
	double volumenLitros;//volumen en litros
	double volumenMetrosCubic;//volumen en metros cubicos
	
	
	//initialize
	a = 0;
	volumen = 0;
	volumenLitros = 0;
	volumenMetrosCubic = 0;
	
	
	//Display phrase
	cout<<"Digite longituf del tonel: ";
	cin>>i;
	cout<<"Digite diametro en los extremos: ";
	cin>>diamExtremos;
	cout<<"Digite diametro en el centro: ";
	cin>>diamCentro;
	
	//operador
	a = (diamExtremos/2)+(2/3)*(diamExtremos/2-diamCentro/2);
	volumen = M_PI*i*pow(a,2);
	volumenLitros = volumen/pow(10,3);
	volumenMetrosCubic = volumen/pow(10,6);
	
	//Display phrase
	cout<<"\n\rVolumen en centimetros cubicos: "<<volumen<<endl;
	cout<<"\n\rVolumen en litros: "<<volumenLitros<<endl;
	cout<<"\n\rVolumen en metros cubicos: "<<volumenMetrosCubic<<endl;
	
	return 0;
	
}
