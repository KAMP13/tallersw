#include <iostream>
#include <math.h>
/* 11.- Hacer un algoritmo para ingresar una medida en metros, 
y que imprima esa medida expresada en centímetros, pulgadas, 
pies y yardas. Los factores de conversión son los siguientes:
1 yarda = 3 pies
1 pie = 12 pulgadas
1 metro = 100 centímetros
1 pulgada = 2.54 centímetros
*/
using namespace std;
int main(){
	
	//Declaration
	double metros, centimetros, pulgadas, pies, yardas;
	
	//initialize
	metros = 0;
	
	//Display phrase
	cout<<"Digite medida en metros: ";
	cin>>metros;
	
	
	//operador
	centimetros = metros*100;
	pulgadas = centimetros/2.54;
	pies = pulgadas/12;
	yardas = pies/3;
	
	
	//Display phrase
	cout<<"\n\rCentimetros: "<<centimetros<<endl;
	cout<<"\n\rPulgadas: "<<pulgadas<<endl;
	cout<<"\n\rPies: "<<pies<<endl;
	cout<<"\n\rYardas: "<<yardas<<endl;
	
	return 0;
	
}
