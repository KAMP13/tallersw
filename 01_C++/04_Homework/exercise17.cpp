#include <iostream>
#include <math.h>
/* 17.-  El �rea de una elipse se obtiene con la f�rmula ?ab , 
donde a es el radio menor de la elipse y b es el radio mayor, 
y su per�metro se obtiene con la f�rmula ?[4(a+b)2]0.5. Realice 
un programa en C ++ utilizando estas f�rmulas y calcule el �rea 
y el per�metro de una elipse que tiene un radio menor de 2.5 cm 
y un radio mayor de 6.4 cm.
*/
using namespace std;
int main(){
	
	//Declaration
	double area,perimetro;
	double a;//radio menor
	double b;//radio mayor
	
	
	//initialize
	a = 2.5;
	b = 6.4;
	
	//Display phrase
	
	//operador
	area = M_PI*a*b;
	perimetro = M_PI*(4*(a+b)*2)*0.5;
	
	
	//Display phrase
	cout<<"\n\rArea: "<<area<<endl;
	cout<<"\n\rPerimetro: "<<perimetro<<endl;
	
	
	return 0;
	
}
