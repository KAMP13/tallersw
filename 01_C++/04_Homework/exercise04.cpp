/**
 * @file CodeReviewExercise04.cpp
 * @author Kevin Martinez (kampzen13@gmail.com)
 * @brief exercise 04:
 		Un departamento de climatolog�a ha realizado recientemente 
		su conversi�n al sistema m�trico. Dise�ar un algoritmo para realizar las siguientes conversiones:
		a. Leer la temperatura dada en la escala Celsius e imprimir en su 
		equivalente Fahrenheit (la f�rmula de conversi�n es "F=9/5 �C+32").
		b. Leer la cantidad de agua en pulgadas e imprimir su equivalente 
		en mil�metros (25.5 mm = 1pulgada.

 * @version 1.0
 * @date 27.11.2021
 * 
 */
 
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
 	int celsius=0;								//Entrada de celsius
 	int fahrenheit=0;							//Entrada de fahrenheit
 	int pulgadas=0;							    //Entrada de pulgadas
 	int milimetros=0;							//Entrada de milimetros
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
	int calcularFahrenheit(int celsius);
	int calcularMilimetros(int pulgadas);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculo en proceso\r\n";
 	cout<<"\tDigite el numero en celsius: ";
 	cin>>celsius;
 	cout<<"\tDigite el numero en pulgadas: ";
 	cin>>pulgadas;
 	
}

//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	fahrenheit = calcularFahrenheit(celsius);
	milimetros = calcularMilimetros(pulgadas); 
}
//=====================================================================================================
//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\tEquivalente en Fahrenheit: "<<fahrenheit<<"\r\n";
 	cout<<"\tEquivalente en Milimetros: "<<milimetros<<"\r\n";
}
//=====================================================================================================
int calcularFahrenheit(int celsius){
	return	(celsius*(9.0/5.0)) + 32;
}
int calcularMilimetros(int pulgadas){
	return	25.5*pulgadas;
}
