#include <iostream>
/*5.- El costo de un autom�vil nuevo para un comprador es 
 la suma total del costo del veh�culo, del porcentaje de la
 ganancia del vendedor y de los impuestos locales o estatales 
 aplicables (sobre el precio de venta). Suponer una ganancia 
 del vendedor del 12% en todas las unidades y un impuesto del 
 6% y dise�ar un algoritmo para leer el costo total del autom�vil
 e imprimir el costo para el consumidor.
*/
using namespace std;
int main(){
	float costVehiculo, gananVendedor, impLocales, costConsumidor;
	
	//initialize
	costVehiculo = 0;
	gananVendedor = 0.12;
	impLocales = 0.6;
	costConsumidor = 0;
	
	//Display phrase 
	cout<<"Digite el costo del Vehiculo para el comprador: ";
	cin>>costVehiculo;
	
	//operador
	gananVendedor = gananVendedor*costVehiculo;
	impLocales = impLocales*costVehiculo;
	costConsumidor = costVehiculo + gananVendedor + impLocales; 
	
	//Display phrase
	cout<<"\n\rCosto para el consumidor: "<<costConsumidor<<endl;
	
	
	
	return 0;
	
}
