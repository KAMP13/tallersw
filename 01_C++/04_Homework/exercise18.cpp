#include <iostream>
#include <math.h>
/* 18.- Si coloca una escalera de 3 metros a un �ngulo 
de 85 grados al lado de un edificio, la altura en la cual 
la escalera toca el edificio se puede calcular como 
altura=3 * seno 85�. Calcule esta altura con una calculadora 
y luego escriba un programa en C que obtenga y visualice el valor de la altura. 
Nota: Los argumentos de todas las funciones trigonom�tricas (seno, coseno, etc) 
deben estar expresados en radianes. Por tanto, para obtener el seno, por ejemplo, 
de un �ngulo expresado en grados, primero deber� convertir el �ngulo a radianes.
*/
using namespace std;
int main(){
	
	//Declaration
	
	double altura;
	double rad;//en radianes
	double angulo;//en grados sexagesimales
	
	
	//initialize
	rad = 0;
	altura = 0;
	
	//Display phrase
	cout<<"Digite el angulo: ";
	cin>>angulo;
	
	//operador
	rad= angulo*M_PI/180;
	altura = 3 * sin(rad);
	
	
	//Display phrase
	cout<<"\n\rALtura: "<<altura<<endl;
	
	
	return 0;
	
}
