#include <iostream>
#include <math.h>
/*7.- Calcular la fuerza de atracci�n entre dos masas, 
separadas por una distancia, mediante la siguiente f�rmula:
F = G*masa1*masa2 / distancia2
Donde G es la constante de gravitaci�n universal: 
G = 6.673 * 10-8 cm3/g.seg2
*/
using namespace std;
int main(){
	
	//Declaration
	double fuerzaAtraccion, consGravitacion, masa1, masa2, 
	distancia;
	
	//initialize
	consGravitacion = 0;
	fuerzaAtraccion = 0;
	
	//Display phrase 
	cout<<"Digite numero de masa 1: ";
	cin>>masa1;
	cout<<"Digite numero de masa 2: ";
	cin>>masa2;
	cout<<"Digite numero de distancia: ";
	cin>>distancia;
	
	//operador
	consGravitacion = 6.673*pow(10,-8);
	fuerzaAtraccion = (consGravitacion*masa1*masa2)/(distancia*distancia);
	
	//Display phrase
	cout<<"\n\rFuerza de Atraccion: "<<fuerzaAtraccion<<endl;
	
	
	return 0;
	
}
