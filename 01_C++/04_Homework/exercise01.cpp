/**
 * @author Kevin Martinez (kampzen13@gmail.com)
		1.- Elabore un algoritmo que dados como datos  
		de entrada el radio y la altura de un cilindro 
		calcular, el �rea lateral y el volumen del cilindro.
  	    A = 2   radio*altura	V =   radio2*altura
 * @version 1.0
 * @date 05.12.2021
 *
*/

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include<iostream>

using namespace std;

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	// Declaration and Initialization of variables
	float terminalInputRadius = 0;			//Entrada de radio a travez del terminal
	float terminalInputHeight = 0; 			//Entrada de altura a travez del terminal
	float circularArea = 0; 				//Area circular calculada
	float volumenCylinder = 0;				//Volumen del cilindro calculado
	
	
	// Ask data throught the terminal
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the radius of the cylinder: ";
	cin>>terminalInputRadius;
	cout<<"\tWrite the height of the cylinder: ";
	cin>>terminalInputHeight;
	
	// Calculation
	circularArea = 2.0*3.14*terminalInputRadius*terminalInputHeight;		//area_circulo = 2 * 3.14 * radio * altura
	volumenCylinder = 3.14*terminalInputRadius*terminalInputHeight;			//volumen_cilindro = 3.14 * radio * altura
	
	// Show results in terminal
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe circular area is: "<<circularArea<<endl;
	cout<<"\tThe volumen is: "<<volumenCylinder<<endl;
	
	return 0;
}
