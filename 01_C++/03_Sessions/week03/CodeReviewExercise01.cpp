/**
 * @file CodeReviewExercise01.cpp
 * @author Kevin Martinez (kampzen13@gmail.com)
 * @brief exercise 01
 * @version 1.0
 * @date 11.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float terminalInputRadius = 0;					//Entrada de radio a travez del terminal
float terminalInputHeight = 0;					//Entrada de altura a travez del terminal
float lateralCylinderArea = 0;					//Area lateral del cilindro calculada
float volumenCylinder = 0;					//Volumen del cilindro calculado

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculateBasicFormula(int terminalInputRadius, int terminalInputHeight);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}
/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the radius of the cylinder: ";
	cin>>terminalInputRadius;
	cout<<"\tWrite the height of the cylinder: ";
	cin>>terminalInputHeight;
}
//=====================================================================================================

void Calculate(){
	
	lateralCylinderArea = 2.0*CalculateBasicFormula(terminalInputRadius, terminalInputHeight);		//area_lateral_cilindro = 2 * 3.14 * radio * altura
	volumenCylinder = terminalInputRadius*CalculateBasicFormula(terminalInputRadius, terminalInputHeight);			//volumen_cilindro = 3.14 * radio * radio * altura

}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe circular area is: "<<lateralCylinderArea<<endl;
	cout<<"\tThe volumen is: "<<volumenCylinder<<endl;
}
//=====================================================================================================

float CalculateBasicFormula(int terminalInputRadius, int terminalInputHeight){
	return((float)terminalInputRadius*(float)terminalInputHeight)*3.14;
}
