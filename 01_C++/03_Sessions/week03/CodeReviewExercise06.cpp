/**
 * @file CodeReviewExercise06.cpp
 * @author Kevin Martinez (kampzen13@gmail.com)
 * @brief exercise 06:
 		Desglosar cierta cantidad de segundos a su equivalente en d�as, horas, minutos y segundos.
 * @version 1.0
 * @date 31.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
	
	int dias;			//salida de dias
	int horas;			//salida de horas
	int minutos;		//salida de minutos
	int segundos;		//entrada de segundos
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calcularsegundos(float segundos);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Digite numero de segundos: ";
	cin>>segundos;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	dias = calcularsegundos(segundos)/24;
	horas = calcularsegundos(segundos)*6;
	minutos = calcularsegundos(segundos)*10;
	segundos = segundos;
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\nEquivalente en Dias: "<<dias<<endl;
	cout<<"\n\rEquivalente en Horas: "<<horas<<endl;
	cout<<"\n\rEquivalente en Minutos: "<<minutos<<endl;
	cout<<"\n\rEquivalente en Segundos: "<<segundos<<endl;
}
//=====================================================================================================

float calcularsegundos(float segundos){
	return	segundos / 600;
}
