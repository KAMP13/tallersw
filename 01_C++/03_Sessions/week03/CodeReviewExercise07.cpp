/**
 * @file CodeReviewExercise07.cpp
 * @author Kevin Martinez (kampzen13@gmail.com)
 * @brief exercise 07:
 		Calcular la fuerza de atracci�n entre dos masas, separadas por una distancia, mediante la siguiente f�rmula:
		F = G*masa1*masa2 / distancia2
		Donde G es la constante de gravitaci�n universal: G = 6.673 * 10-8 cm3/g.seg2

 * @version 1.0
 * @date 31.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
	
	double fuerzaAtraccion;
	double consGravitacion;
	double masa1;
	double masa2;
	double distancia;
	float cons=6.673;
	
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float consGravitacionn(float cons);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Digite numero de masa 1: ";
	cin>>masa1;
	cout<<"Digite numero de masa 2: ";
	cin>>masa2;
	cout<<"Digite numero de distancia: ";
	cin>>distancia;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	fuerzaAtraccion = (consGravitacionn(cons)*masa1*masa2)/(distancia*distancia);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\n\rFuerza de Atraccion: "<<fuerzaAtraccion<<endl;
}
//=====================================================================================================

float consGravitacionn(float cons){
	return	cons * pow(10,-8);
}
