/**
 * @file CodeReviewExercise05.cpp
 * @author Kevin Martinez (kampzen13@gmail.com)
 * @brief exercise 05:
 		El costo de un autom�vil nuevo para un comprador es 
 		la suma total del costo del veh�culo, del porcentaje de la
		ganancia del vendedor y de los impuestos locales o estatales 
 		aplicables (sobre el precio de venta). Suponer una ganancia 
		del vendedor del 12% en todas las unidades y un impuesto del 
		6% y dise�ar un algoritmo para leer el costo total del autom�vil
		e imprimir el costo para el consumidor.
 * @version 1.0
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
	float costVehiculo= 0;							//Entrada de costo de vehiculo
	float gananVendedor= 0.12;						//Ganancia de vendedor
	float impLocales= 0.6;							//impuestos locales
	float costConsumidor= 0;						//costo de consumidor
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calcularGananVendedor(float gananVendedor, float costVehiculo);
double calcularImLocales(double impLocales, double costVehiculo);
double calcularCostConsumidor(double impLocales, double costVehiculo, double gananVendedor);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tIngresa el costo del vehiculo: ";
 	cin>>costVehiculo;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	gananVendedor = calcularGananVendedor(gananVendedor,costVehiculo);
	impLocales = calcularImLocales(impLocales, costVehiculo);
	costConsumidor = calcularCostConsumidor(impLocales, costVehiculo, gananVendedor); 
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\tCosto para el consumidor: "<<costConsumidor<<"\r\n";
}
//=====================================================================================================

float calcularGananVendedor(float gananVendedor, float costVehiculo){
	return	gananVendedor * costVehiculo;
}
double calcularImLocales(double impLocales, double costVehiculo){
	return	impLocales * costVehiculo;
}
double calcularCostConsumidor(double impLocales, double costVehiculo, double gananVendedor){
	return	impLocales + costVehiculo + gananVendedor;
}
