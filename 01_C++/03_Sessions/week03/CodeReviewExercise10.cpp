/**
 * @file CodeReviewExercise10.cpp
 * @author Kevin Martinez (kampzen13@gmail.com)
 * @brief exercise 10:
 		Un millonario exc�ntrico ten�a tres hijos: Carlos, Jos� y Marta. 
		Al morir dej� el siguiente legado: A Jos� le dej� 4/3 de lo que 
		le dej� a Carlos. 
		A Carlos le dej� 1/3 de su fortuna. A Marta le dejo la mitad de 
		lo que le dej� a Jos�. 
		Preparar un algoritmo para darle la suma a repartir e imprima 
		cuanto le toc� a cada uno.
  
 * @version 1.0
 * @date 11.02.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
	
	double carlos=0;					//salida de montos
	double jose=0;						//ingreso de capital
	double marta=0;						//ingreso de tipo de interes
	double total=0;						//ingreso de numero de periodos
					
	
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double	calculateCarlos(double total);
double	calculateJose(double carlos);
double	calculateMarta(double jose);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Digite total de la herencia: ";
	cin>>total;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	calculateCarlos(total);
	calculateJose(carlos);
	calculateMarta(jose);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\n\rCARLOS: "<<carlos<<endl;
	cout<<"\n\rJOSE: "<<jose<<endl;
	cout<<"\n\rMARTA: "<<marta<<endl;
}
//=====================================================================================================
double calculateCarlos(double total){
	carlos=total/3;
	return	carlos;
}
double calculateJose(double carlos){
	jose=(4*carlos)/3;
	return	jose;
}
double calculateMarta(double jose){
	marta=(jose/2);
	return	marta;
}
