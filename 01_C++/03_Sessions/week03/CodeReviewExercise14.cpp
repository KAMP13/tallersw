/**
 * @file CodeReviewExercise14.cpp
 * @author Kevin Mart�nez (kampzen13@gmail.com)
 * @brief exercise 14:
 			Modifique el programa anterior para que, suponiendo que 
			las medidas de entrada son dadas en cent�metros, el resultado lo 
			muestre en: litros, cent�metros c�bicos y metros c�bicos. 
			Recuerde que 1 litro es equivalente a un dec�metro c�bico. 
			Indique siempre la unidad de medida empleada.
 * @version 1.0
 * @date 11.02.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
	
	double volumen=0;
	double i;//longitud      
	double a=0;//contanste
	double diamExtremos;//diametro de los extremos
	double diamCentro;//diametro del centro
	double volumenLitros=0;//volumen en litros
	double volumenMetrosCubic=0;//volumen en metros cubicos

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double calculateVolumen(double diamExtremos, double diamCentro);
double calculateVolumenLitros(double diamExtremos, double diamCentro);
double calculateVolumenMetrosCubic(double diamExtremos, double diamCentro);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Digite longitud del tonel: ";
	cin>>i;
	cout<<"Digite diametro en los extremos: ";
	cin>>diamExtremos;
	cout<<"Digite diametro en el centro: ";
	cin>>diamCentro;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	calculateVolumen(diamExtremos, diamCentro);
	calculateVolumenLitros(diamExtremos, diamCentro);
	calculateVolumenMetrosCubic(diamExtremos, diamCentro);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\n\rVolumen: "<<volumen<<endl;
 	cout<<"\n\rVolumen en litros: "<<volumenLitros<<endl;
	cout<<"\n\rVolumen en metros cubicos: "<<volumenMetrosCubic<<endl;

	
}
//=====================================================================================================
double calculateVolumen(double diamExtremos, double diamCentro){
	a = (diamExtremos/2)+(2/3)*(diamExtremos/2-diamCentro/2);
	volumen = M_PI*i*pow(a,2);

	return	volumen;
}
double calculateVolumenLitros(double diamExtremos, double diamCentro){
	volumenLitros = volumen/pow(10,3);

	return	volumenLitros;
}
double calculateVolumenMetrosCubic(double diamExtremos, double diamCentro){
	volumenMetrosCubic = volumen/pow(10,6);

	return	volumenMetrosCubic;
}
