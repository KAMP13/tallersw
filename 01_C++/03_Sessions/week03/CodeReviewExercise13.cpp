/**
 * @file CodeReviewExercise13.cpp
 * @author Kevin Mart�nez (kampzen13@gmail.com)
 * @brief exercise 13:
 			Un tonel es un recipiente, generalmente de madera, 
			muy utilizado para almacenar y mejorar un vino. La forma de 
			un tonel es muy caracter�stica y es un cilindro en el que 
			la parte central es m�s gruesa, es decir, tiene un di�metro 
			mayor que los extremos. Escriba un programa que lea las medidas 
			de un tonel y nos devuelva su
			capacidad, teniendo en cuenta que el volumen (V) de un tonel 
			viene dado por la siguiente f�rmula: V = ? l a2 donde:
 			l    es la longitud del tonel, su altura. a = d/2 + 2/3(D/2 - d/2)
			d   es el di�metro del tonel en sus extremos.
			D  es el di�metro del tonel en el centro: D>d
Nota: Observe que si las medidas se dan en cent�metros 
el resultado lo obtenemos en cent�metros c�bicos.

 * @version 1.0
 * @date 11.02.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
	
	double volumen=0;					//salida de montos
	double i;						//ingreso de capital
	double a=0;						//ingreso de tipo de interes
	double diamExtremos;
	double diamCentro;
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double calculateVolumen(double diamExtremos, double diamCentro);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Digite longitud del tonel: ";
	cin>>i;
	cout<<"Digite diametro en los extremos: ";
	cin>>diamExtremos;
	cout<<"Digite diametro en el centro: ";
	cin>>diamCentro;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	calculateVolumen(diamExtremos,diamCentro);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\n\rVolumen: "<<volumen<<endl;
	
}
//=====================================================================================================
double calculateVolumen(double diamExtremos, double diamCentro){
	a = (diamExtremos/2)+(2/3)*(diamExtremos/2-diamCentro/2);
	volumen = M_PI*i*pow(a,2);

	return	volumen;
}
