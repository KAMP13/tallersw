/**
 * @file CodeReviewExercise08.cpp
 * @author Kevin Martinez (kampzen13@gmail.com)
 * @brief exercise 09:
 		�Cu�l es el capital que debe colocarse a inter�s  
		 compuesto del 8%  anual para que despu�s de 20 
		 a�os produzca un monto de $ 500,000. ?  
 * @version 1.0
 * @date 11.02.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
	
	double monto=0;					//salida de montos
	double capital;					//ingreso de capital
	double tipoDeInteres;			//ingreso de tipo de interes
	double numeroDePeriodos;		//ingreso de numero de periodos
	double numeroDeAnios=0;			//ingreso de numero de a�os
	double porAnios;				//salida de monto por a�o
	double interes;					
	
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double calculatetipoDeInteres(double interes);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Digite tipo de interes en porcentaje: ";
	cin>>tipoDeInteres;
	cout<<"Digite capital: ";
	cin>>capital;
	cout<<"Digite numero de periodos: ";
	cin>>numeroDePeriodos;
	cout<<"Digite numero de a�os: ";
	cin>>numeroDeAnios;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	monto = capital*(1+(calculatetipoDeInteres(interes)/numeroDePeriodos));
	numeroDeAnios = monto * numeroDeAnios;
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\n\rmonto: "<<monto<<endl;
	cout<<"\n\rmonto por A�os: "<<numeroDeAnios<<endl;
}
//=====================================================================================================
double calculatetipoDeInteres(double interes){
	return	interes/100;
}
