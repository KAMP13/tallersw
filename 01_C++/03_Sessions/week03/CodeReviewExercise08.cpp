/**
 * @file CodeReviewExercise08.cpp
 * @author Kevin Martinez (kampzen13@gmail.com)
 * @brief exercise 08:
 		Calcular el monto final, dados como datos el Capital 
		Inicial, el  tipo de Inter�s, el numero de periodos por a�o, 
		y el numero de a�os de  la inversi�n. El c�lculo del capital 
		final se basa en la formula del inter�s compuesto.
				M = C(1+i/N)
		Donde:
		M = Capital final o Monto,	C = Capital Inicial,	
		i = Tipo de inter�s nominal
		N = Numero de periodos por a�o,		T = Numero de a�os
		- Si un cliente deposita al Banco la cantidad de $10,000 a 
		inter�s compuesto con una tasa del 8% anual.  �Cual ser� el 
		monto que recaude despu�s de 9 a�os? 
		 - �Cuanto debe cobrar el cliente dentro de 3 a�os si 
 		deposita  $ 100,000. al 9% anual  y capitaliz�ndose los 
 		intereses bimestralmente?  
 * @version 1.0
 * @date 31.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
	
	double monto;					//salida de montos
	double capital;					//ingreso de capital
	double tipoDeInteres;			//ingreso de tipo de interes
	double numeroDePeriodos;		//ingreso de numero de periodos
	double numeroDeAnios;			//ingreso de numero de a�os
	double porAnios;				//salida de monto por a�o
	double interes;					
	
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double calculatetipoDeInteres(double interes);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Digite tipo de interes en porcentaje: ";
	cin>>tipoDeInteres;
	cout<<"Digite capital: ";
	cin>>capital;
	cout<<"Digite numero de periodos: ";
	cin>>numeroDePeriodos;
	cout<<"Digite numero de a�os: ";
	cin>>numeroDeAnios;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	monto = capital*(1+(calculatetipoDeInteres(interes)/numeroDePeriodos));
	numeroDeAnios = monto * numeroDeAnios;
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\n\rmonto: "<<monto<<endl;
	cout<<"\n\rmonto por A�os: "<<numeroDeAnios<<endl;
}
//=====================================================================================================

double calculatetipoDeInteres(double interes){
	return	interes/100;
}
