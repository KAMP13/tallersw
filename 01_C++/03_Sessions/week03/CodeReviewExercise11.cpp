/**
 * @file CodeReviewExercise11.cpp
 * @author Kevin Martinez (kampzen13@gmail.com)
 * @brief exercise 11:
 			Hacer un algoritmo para ingresar una medida en metros, 
			y que imprima esa medida expresada en centímetros, pulgadas, 
			pies y yardas. Los factores de conversión son los siguientes:
			1 yarda = 3 pies
			1 pie = 12 pulgadas
			1 metro = 100 centímetros
			1 pulgada = 2.54 centímetros

  
 * @version 1.0
 * @date 11.02.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
	
	double metros=0;					//salida de montos
	double centimetros;						//ingreso de capital
	double pulgadas;						//ingreso de tipo de interes
	double pies;						//ingreso de numero de periodos
	double yardas;				
	
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double calculateCentimetros(double metros);
double calculatePulgadas(double centimetros);
double calculatePies(double pulgadas);
double calculateYardas(double pies);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Digite la medida en metros: ";
	cin>>metros;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	calculateCentimetros(metros);
	calculatePulgadas(centimetros);
	calculatePies(pulgadas);
	calculateYardas(pies);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\n\rCentimetros: "<<centimetros<<endl;
	cout<<"\n\rPulgadas: "<<pulgadas<<endl;
	cout<<"\n\rPies: "<<pies<<endl;
	cout<<"\n\rYardas: "<<yardas<<endl;
}
//=====================================================================================================
double calculateCentimetros(double metros){
	centimetros=metros*100;
	return	centimetros;
}
double calculatePulgadas(double centimetros){
	pulgadas=centimetros/2.54;
	return	pulgadas;
}
double calculatePies(double pulgadas){
	pies=pulgadas/12;
	return	pies;
}
double calculateYardas(double pies){
	yardas=pies/3;
	return	yardas;
}
