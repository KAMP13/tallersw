/**
 * @file CodeReviewExercise12.cpp
 * @author Kevin Martinez (kampzen13@gmail.com)
 * @brief exercise 12:
 			Escriba un programa para calcular el tiempo transcurrido, 
			en minutos, necesario para hacer un viaje. 
			La ecuacin es tiempo transcurrido = distancia total/velocidad promedio. 
			Suponga que la distancia est en kilmetros y la velocidad en kilmetros/hora.
 * @version 1.0
 * @date 11.02.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
	
	double tiempo=0;					//salida de montos
	double distTotal;						//ingreso de capital
	double veloPromedio;						//ingreso de tipo de interes
	double tiempoMin;
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double calculateTiempo(double distTotal, double veloPromedio);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Digite la distancia total: ";
	cin>>distTotal;
	cout<<"Digite la velocidad promedio: ";
	cin>>veloPromedio;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	calculateTiempo(distTotal,veloPromedio);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"\n\rTiempo en minutos: "<<tiempoMin<<endl;
	
}
//=====================================================================================================
double calculateTiempo(double distTotal, double veloPromedio){
	tiempo=distTotal/veloPromedio;
	tiempoMin=tiempo*60;
	return	tiempoMin;
}
